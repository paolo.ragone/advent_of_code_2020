var fs = require('fs');
var lines = fs.readFileSync('6_input.txt').toString().split("\n");

// Let's process the file
const groups = [];
var currentGroup = undefined;
for (line of lines) {
    if (line.trim().length === 0) {
        groups.push(currentGroup);
        currentGroup = undefined;
    } else {
        var currentLine = new Set();
        for (const letter of line) {
            currentLine.add(letter);
        }
        if (currentGroup === undefined) {
            currentGroup = currentLine;
        } else {
            currentGroup = new Set([...currentGroup].filter(l => currentLine.has(l)));
        }
    }
}
if (currentGroup !== undefined) {
    groups.push(currentGroup);
}


console.log(`Groups: `, groups[0], groups[1]);
const totalCount = groups.reduce((prev, group) => prev + group.size, 0);
console.log(`Total count: ${totalCount}`);