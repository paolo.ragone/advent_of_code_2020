var fs = require('fs');
var lines = fs.readFileSync('4_input.txt').toString().split("\n");

// Let's process the file
const passports = [];
currentPassport = {}
for (line of lines) {
    if (line.trim().length === 0) {
        passports.push(currentPassport);
        currentPassport = {};
    }
    const parts = line.trim().split(' ');
    for (const part of parts) {
        if (part.length > 0) {
            const sub = part.split(':', 2);
            currentPassport[sub[0]] = sub[1];
        }
    }
}

// byr (Birth Year)
// iyr (Issue Year)
// eyr (Expiration Year)
// hgt (Height)
// hcl (Hair Color)
// ecl (Eye Color)
// pid (Passport ID)
// cid (Country ID)
function isValidPassport(passport, cidOptional = true) {
    return 'byr' in passport &&
        'iyr' in passport &&
        'eyr' in passport &&
        'hgt' in passport &&
        'hcl' in passport &&
        'ecl' in passport &&
        'pid' in passport &&
        (cidOptional || 'cid' in passport);
}

console.log(`Found ${passports.length} passports`);
const validOnes = passports.filter(p => isValidPassport(p));
console.log(`${validOnes.length} valid ones`);
