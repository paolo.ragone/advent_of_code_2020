var fs = require('fs');
var lines = fs.readFileSync('6_input.txt').toString().split("\n");

// Let's process the file
const groups = [];
var currentGroup = new Set();
for (line of lines) {
    if (line.trim().length === 0) {
        groups.push(currentGroup);
        currentGroup = new Set();
    }
    for (const letter of line) {
        currentGroup.add(letter);
    }
}
if (currentGroup.size > 0) {
    groups.push(currentGroup);
}


console.log(`Groups: `, groups[0], groups[0].size);
const totalCount = groups.reduce((prev, group) => prev + group.size, 0);
console.log(`Total count: ${totalCount}`);