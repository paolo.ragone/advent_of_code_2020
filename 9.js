const { exec } = require('child_process');
var fs = require('fs');
const { start } = require('repl');
var lines = fs.readFileSync('9_input.txt').toString().split("\n");
var numbers = lines.map(l => parseInt(l, 10));

const pre25 = numbers.slice(0, 25);
var positionToCheck = 25;
var numberToCheck = numbers[positionToCheck];

function canBeAddition(num, numList) {
    const copyList = [...numList];
    copyList.sort((a,b) => a < b ? -1 : a > b ? 1 : 0);
    var i = 0;
    var j = copyList.length - 1;
    while (i<j) {
        const add = copyList[i] + copyList[j];
        if (add === num) {
            return true;
        }
        if (add < num) {
            i++;
        }
        if (add > num) {
            j--;
        }
    }
    return false;
}

while (positionToCheck < numbers.length && canBeAddition(numberToCheck, pre25)) {
    pre25.shift()
    pre25.push(numberToCheck);
    positionToCheck++;
    numberToCheck = numbers[positionToCheck];
}
console.log('Position: ', positionToCheck, 'number: ', numberToCheck, 'can\'t be an addition of any 2 numbers of', pre25);

const invalidNumber = numberToCheck;
const maxPosition = positionToCheck - 1;

for (var startPos = 0; startPos < (maxPosition - 1); startPos++) {
    var acum = 0;
    var endPos = startPos;
    while (acum < invalidNumber) {
        acum = acum + numbers[endPos];
        endPos++;
    }
    if (acum === invalidNumber) {
        const range = numbers.slice(startPos, endPos);
        range.sort((a,b) =>  a< b ? -1 : a > b ? 1 : 0);
        console.log(`Adding numbers from pos ${startPos} (${numbers[startPos]}) to pos ${endPos} (${numbers[endPos]}) works.`);
        console.log(`Smaller number in range is ${range[0]}, largest is ${range[range.length - 1]} ==> Weakness is: ${range[0] + range[range.length - 1]}`);
        break;
    }
}