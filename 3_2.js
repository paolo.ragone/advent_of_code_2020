var fs = require('fs');
var lines = fs.readFileSync('3_input.txt').toString().split("\n");

function parseLine(line) {
    const resp = [];
    for (i of line) {
        resp.push(i === '#');
    }
    return resp;
}

var map = lines.map(parseLine);

function isTree(x, y) {
    const row = map[y];
    return row[x % row.length];
}

// console.log(lines[0])
// console.log(map[0])

function checkSlope(dx, dy) {
    x = 0;
    y = 0;
    var counter = 0;
    while (y < map.length) {
        if (isTree(x,y)) counter++;
        x = x + dx;
        y = y + dy;
    }
    console.log(`With slope right ${dx}, down ${dy} we encounter ${counter} trees`);
    return counter;
}

const trees = [
    checkSlope(1,1),
    checkSlope(3,1),
    checkSlope(5,1),
    checkSlope(7,1),
    checkSlope(1,2)
];
const mult = trees.reduce((p, v) => p*v,1);
console.log(`Multiplication ${mult}`);
