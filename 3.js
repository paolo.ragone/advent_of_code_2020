var fs = require('fs');
var lines = fs.readFileSync('3_input.txt').toString().split("\n");

function parseLine(line) {
    const resp = [];
    for (i of line) {
        resp.push(i === '#');
    }
    return resp;
}

var map = lines.map(parseLine);

function isTree(x, y) {
    const row = map[y];
    return row[x % row.length];
}

// console.log(lines[0])
// console.log(map[0])

x = 0;
y = 0;
var counter = 0;
while (y < map.length) {
    if (isTree(x,y)) counter++;
    x = x + 3;
    y++;
}

console.log(`Found ${counter} trees`);