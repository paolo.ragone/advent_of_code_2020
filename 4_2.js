var fs = require('fs');
var lines = fs.readFileSync('4_input.txt').toString().split("\n");

// Let's process the file
const passports = [];
currentPassport = {}
for (line of lines) {
    if (line.trim().length === 0) {
        passports.push(currentPassport);
        currentPassport = {};
    }
    const parts = line.trim().split(' ');
    for (const part of parts) {
        if (part.length > 0) {
            const sub = part.split(':', 2);
            currentPassport[sub[0]] = sub[1];
        }
    }
}

function validNum(val, length, min, max) {
    if (val.length !== length) return false;
    const valNumber = parseInt(val, 10);
    return valNumber >= min && valNumber <= max;
}
function validHeight(hgt) {
    const num = parseInt(hgt.substr(0, hgt.length-2), 10);
    if (hgt.endsWith('in')) {
        return num >= 59 && num <= 76;
    }
    if (hgt.endsWith('cm')) {
        return num >= 150 && num <= 193;
    }
    return false;
}
// byr (Birth Year)
// iyr (Issue Year)
// eyr (Expiration Year)
// hgt (Height)
// hcl (Hair Color)
// ecl (Eye Color)
// pid (Passport ID)
// cid (Country ID)
function isValidPassport(passport, cidOptional = true) {
    return 'byr' in passport && validNum(passport.byr, 4, 1920, 2002) &&
        'iyr' in passport && validNum(passport.iyr, 4, 2010, 2020) &&
        'eyr' in passport && validNum(passport.eyr, 4, 2020, 2030) &&
        'hgt' in passport && /^[0-9]+(cm|in)$/.test(passport.hgt) && validHeight(passport.hgt) &&
        'hcl' in passport && /^#[0-9a-f]{6}$/.test(passport.hcl) &&
        'ecl' in passport && ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].indexOf(passport.ecl) >= 0 &&
        'pid' in passport && /^[0-9]{9}$/.test(passport.pid) &&
        (cidOptional || 'cid' in passport);
}

console.log(`Found ${passports.length} passports`);
const validOnes = passports.filter(p => isValidPassport(p));
console.log(`${validOnes.length} valid ones`);
