var fs = require('fs');
var lines = fs.readFileSync('2_input.txt').toString().split("\n");

// console.log(lines.length)
// console.log(lines[0])
// for(const line of lines) {
//     console.log(line);
// }

function isPasswordValid(line) {
    // console.log(line);
    const parts = line.split(' ', 3);
    const fromToParts = parts[0].split('-', 2);
    const from = parseInt(fromToParts[0], 10);
    const to = parseInt(fromToParts[1], 10);
    const letter = parts[1][0];
    const password = parts[2];
    var numLetters = 0;
    for (i of password) {
        if (i === letter) numLetters++
    }
    // const numLetters = password filter(l => l === letter).length;
    const valid = from <= numLetters && numLetters <= to;
    console.log(`${from} -- ${to} -- ${letter} -- ${password} -- ${numLetters} -- ${valid}`);
    return valid;
}

const numValidPasswords = lines.filter(isPasswordValid).length;
console.log(`${numValidPasswords} valid passwords`);