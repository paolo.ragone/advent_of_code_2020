const { exec } = require('child_process');
var fs = require('fs');
var lines = fs.readFileSync('8_input.txt').toString().split("\n");

function executeProgram(source) {
    var globalAcum = 0;
    var execPos = 0;
    var visitedInstructions = [];

    while (execPos < source.length) {
        if (execPos < 0) {
            // console.log('Index out of bounds');
            return {success: false, acc: globalAcum, err: 'out of bounds'};
        }
        if (visitedInstructions.indexOf(execPos) >= 0) {
            // console.log('Infinite loop detected!');
            return {success: false, acc: globalAcum, err: 'infinite loop'};
        }
        const currentPos = execPos;
        visitedInstructions.push(currentPos);
        const input = parseInt(source[currentPos].substr(4));
        switch (source[currentPos].substr(0,3)) {
            case 'nop':
                execPos++;
                break;
            case 'acc':
                globalAcum += input;
                execPos++;
                break;
            case 'jmp':
                execPos += input;
                break;
            default:
                return {success: false, acc: globalAcum, err: 'Unknown command: ' + source[currentPos] };
        }
        // console.log(`${currentPos} --> ${source[currentPos]} | ==> ACC: ${globalAcum} NXT: ${execPos}`);
    }
    if (execPos === source.length) {
        return {success: true, acc: globalAcum};;
    }
    return {success: false, acc: globalAcum, err: 'bad end position ' + execPos};
}

const initialResp = executeProgram(lines);
console.log(`Executing code is ${initialResp.success ? 'successful' : 'unsuccessful because of ' + initialResp.err}. Acum is ${initialResp.acc}`);


var changeIndex = 0;
while (changeIndex < lines.length) {
    if (lines[changeIndex].substr(0,3) === 'nop') {
        const mutated = [...lines];
        mutated[changeIndex] = 'jmp' + lines[changeIndex].substr(3);
        const resp = executeProgram(mutated);
        if (resp.success) {
            console.log(`By changing line ${changeIndex} from '${lines[changeIndex]}' to '${mutated[changeIndex]}', the code finishes with an acum of ${resp.acc}`);
            break;
        }
    }
    if (lines[changeIndex].substr(0,3) === 'jmp') {
        const mutated = [...lines];
        mutated[changeIndex] = 'nop' + lines[changeIndex].substr(3);
        const resp = executeProgram(mutated);
        if (resp.success) {
            console.log(`By changing line ${changeIndex} from '${lines[changeIndex]}' to '${mutated[changeIndex]}', the code finishes with an acum of ${resp.acc}`);
            break;
        }
    }
    changeIndex++;
}