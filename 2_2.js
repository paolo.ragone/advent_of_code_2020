var fs = require('fs');
var lines = fs.readFileSync('2_input.txt').toString().split("\n");

// console.log(lines.length)
// console.log(lines[0])
// for(const line of lines) {
//     console.log(line);
// }

function isPasswordValid(line) {
    // console.log(line);
    const parts = line.split(' ', 3);
    const fromToParts = parts[0].split('-', 2);
    const pos1 = parseInt(fromToParts[0], 10);
    const pos2 = parseInt(fromToParts[1], 10);
    const letter = parts[1][0];
    const password = parts[2];
    const valid = ((password[pos1 - 1] === letter) || (password[pos2 - 1] === letter)) && (password[pos1 - 1] !== password[pos2 - 1]);
    // console.log(`${pos1} (${password[pos1 - 1]}) -- ${pos2} (${password[pos2 - 1]}) -- ${letter} -- ${password} -- ${valid}`);
    return valid;
}

const numValidPasswords = lines.filter(isPasswordValid).length;
console.log(`${numValidPasswords} valid passwords`);