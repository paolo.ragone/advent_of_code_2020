var fs = require('fs');
var lines = fs.readFileSync('7_input.txt').toString().split("\n");


class BagColour {
    constructor(name) {
        this.name = name;
        this.contents = []; // {amount: int, bag: BagColour}[]
        this.containingColours = []; // BagColour[]
    }
    addContent(amount, colour) {
        const bag = getBagColour(colour);
        this.contents.push({amount, bag });
        bag.addContainingColour(this);
    }
    addContainingColour(colour) {
        this.containingColours.push(colour);
    }
    findAllContainingColours() {
        const resp = this.visitAllContainingColours(new Set());
        resp.delete(this.name);
        return resp;
    }
    visitAllContainingColours(visited) {
        visited.add(this.name);
        for (const bag of this.containingColours) {
            if (!visited.has(bag.name)) {
                bag.visitAllContainingColours(visited);
            }
        }
        return visited;
    }
    countSubBags() {
        return this.contents.reduce((acum, content) => acum + content.amount + content.amount * content.bag.countSubBags(), 0);
    }
}

const allBagColours = new Map();
function getBagColour(name) {
    if (name === '') {
        console.log('HERE');
    }
    if (allBagColours.has(name)) {
        return allBagColours.get(name);
    }
    const bag = new BagColour(name);
    allBagColours.set(name, bag);
    return bag;
}

// Let's process the file
for (line of lines) {
    const [name, contentsStr] = line.split(' bags contain ', 2);
    const bag = getBagColour(name);
    if (!contentsStr.startsWith('no other bags')) {
        const contents = contentsStr.split(', ');
        for (const content of contents) {
            const pos = content.indexOf(' ');
            const amount = content.substr(0, pos);
            const colour = content.substr(pos+1, content.lastIndexOf(' ') - pos - 1);
            bag.addContent(parseInt(amount, 10), colour);
        }
    } else {
        console.log('Skipping');
    }
}

const shinyGold = getBagColour('shiny gold');
const containingColours = shinyGold.findAllContainingColours();
console.log('Colours containing shiny gold: ', containingColours.size, containingColours);

console.log('Sub Bags of shiny gold: ', shinyGold.countSubBags());