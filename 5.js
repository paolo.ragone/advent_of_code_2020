var fs = require('fs');

var lines = fs.readFileSync('5_input.txt').toString().split("\n");

// // Let's process the file
// const passports = [];
// currentPassport = {}
// for (line of lines) {
//     if (line.trim().length === 0) {
//         passports.push(currentPassport);
//         currentPassport = {};
//     }
//     const parts = line.trim().split(' ');
//     for (const part of parts) {
//         if (part.length > 0) {
//             const sub = part.split(':', 2);
//             currentPassport[sub[0]] = sub[1];
//         }
//     }
// }

function binaryToRowNum(str) {
    const str1 = str.toUpperCase();
    return (str1[0] === 'F' ? 0 : 64) + 
        (str1[1] === 'F' ? 0 : 32) + 
        (str1[2] === 'F' ? 0 : 16) + 
        (str1[3] === 'F' ? 0 : 8) + 
        (str1[4] === 'F' ? 0 : 4) + 
        (str1[5] === 'F' ? 0 : 2) + 
        (str1[6] === 'F' ? 0 : 1)
}

function binaryToColNum(str) {
    const str1 = str.toUpperCase();
    return (str1[7] === 'L' ? 0 : 4) + 
        (str1[8] === 'L' ? 0 : 2) + 
        (str1[9] === 'L' ? 0 : 1)
}

function binaryToCoordinate(str) {
    const row = binaryToRowNum(str);
    const col = binaryToColNum (str);
    return {str, row, col, id: row*8+col };
}

console.log(`FDFDDFFRLR => ${JSON.stringify(binaryToCoordinate('FDFDDFFRLR'))}`);
console.log(`BFFFBBFRRR => ${JSON.stringify(binaryToCoordinate('BFFFBBFRRR'))}`);
console.log(`FFFBBBFRRR => ${JSON.stringify(binaryToCoordinate('FFFBBBFRRR'))}`);
console.log(`BBFFBBFRLL => ${JSON.stringify(binaryToCoordinate('BBFFBBFRLL'))}`);

const processed = lines.map(binaryToCoordinate);

const maxId = processed.reduce((prev, cur) => (prev.id >= cur.id ? prev : cur));
console.log('Max id is achieved by', maxId);

const seats = [];
for (var i = 0; i < 128; i++) { seats[i] = 'OOOOOOOO'; }
processed.forEach(c => seats[c.row] = seats[c.row].substr(0, c.col) + 'X' + seats[c.row].substr(c.col + 1));
for (var i = 0; i < 128; i++) {
    console.log(i, seats[i]);
}

// he outcome looks:
/*
0 OOOOOOOO
1 OOOXXXXX
2 XXXXXXXX
3 XXXXXXXX
4 XXXXXXXX
5 XXXXXXXX
6 XXXXXXXX
7 XXXXXXXX
8 XXXXXXXX
9 XXXXXXXX
10 XXXXXXXX
11 XXXXXXXX
12 XXXXXXXX
13 XXXXXXXX
14 XXXXXXXX
15 XXXXXXXX
16 XXXXXXXX
17 XXXXXXXX
18 XXXXXXXX
19 XXXXXXXX
20 XXXXXXXX
21 XXXXXXXX
22 XXXXXXXX
23 XXXXXXXX
24 XXXXXXXX
25 XXXXXXXX
26 XXXXXXXX
27 XXXXXXXX
28 XXXXXXXX
29 XXXXXXXX
30 XXXXXXXX
31 XXXXXXXX
32 XXXXXXXX
33 XXXXXXXX
34 XXXXXXXX
35 XXXXXXXX
36 XXXXXXXX
37 XXXXXXXX
38 XXXXXXXX
39 XXXXXXXX
40 XXXXXXXX
41 XXXXXXXX
42 XXXXXXXX
43 XXXXXXXX
44 XXXXXXXX
45 XXXXXXXX
46 XXXXXXXX
47 XXXXXXXX
48 XXXXXXXX
49 XXXXXXXX
50 XXXXXXXX
51 XXXXXXXX
52 XXXXXXXX
53 XXXXXXXX
54 XXXXXXXX
55 XXXXXXXX
56 XXXXXXXX
57 XXXXXXXX
58 XXXXXXXX
59 XXXXXXXX
60 XXXXXXXX
61 XXXXXXXX
62 XXXXXXXX
63 XXXXXXXX
64 XXXXXXXX
65 XXXXXXXX
66 XXXXXXXX
67 XXXXXXXX
68 XXXXXXXX
69 XXXXXXXX
70 XXXXXXXX
71 XXXXXXXX
72 XXXXXXXX
73 XXXXXXXX
74 XXXXXXXO
75 XXXXXXXX
76 XXXXXXXX
77 XXXXXXXX
78 XXXXXXXX
79 XXXXXXXX
80 XXXXXXXX
81 XXXXXXXX
82 XXXXXXXX
83 XXXXXXXX
84 XXXXXXXX
85 XXXXXXXX
86 XXXXXXXX
87 XXXXXXXX
88 XXXXXXXX
89 XXXXXXXX
90 XXXXXXXX
91 XXXXXXXX
92 XXXXXXXX
93 XXXXXXXX
94 XXXXXXXX
95 XXXXXXXX
96 XXXXXXXX
97 XXXXXXXX
98 XXXXXXXX
99 XXXXXXXX
100 XXXXXXXX
101 XXXXXXXX
102 XXXXXXXX
103 XXXXXXXX
104 XXXXXXXX
105 XXXXXXXX
106 XXXOOOOO
107 OOOOOOOO
108 OOOOOOOO
109 OOOOOOOO
110 OOOOOOOO
111 OOOOOOOO
112 OOOOOOOO
113 OOOOOOOO
114 OOOOOOOO
115 OOOOOOOO
116 OOOOOOOO
117 OOOOOOOO
118 OOOOOOOO
119 OOOOOOOO
120 OOOOOOOO
121 OOOOOOOO
122 OOOOOOOO
123 OOOOOOOO
124 OOOOOOOO
125 OOOOOOOO
126 OOOOOOOO
127 OOOOOOOO

So the answer is 74*8+7
*/